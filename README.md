# Coupon code validator server
This is an apollo graphql server build on top of prisma ORM, and backed up by postgreSQL database 
## Pre-requisites
- Node and npm installed
- Two separate postgres databases 
## To run this project locally 
- Clone this repo
- Create a .env file and add `DATABASE_URL`, and `SHADOW_DATABASE_URL` variables
- Note that prisma uses shadow database to track migration changes into the main database. pre-requisitely you may need two postgres database up locally or remotely
- Install npm packages (`npm i`)
- And run the server in development mode using 'npm run dev'