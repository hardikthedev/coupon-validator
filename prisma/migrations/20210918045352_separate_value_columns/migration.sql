/*
  Warnings:

  - You are about to drop the column `value` on the `coupons` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "coupons" DROP COLUMN "value",
ADD COLUMN     "flatValue" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN     "percentValue" INTEGER NOT NULL DEFAULT 0;
