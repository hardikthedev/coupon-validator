import * as path from 'path'
import { fileLoader } from 'merge-graphql-schemas'

const middlewareArray = fileLoader(path.join(__dirname, './*.middlewares.*'))

export default middlewareArray