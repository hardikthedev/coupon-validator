import { createCouponMiddleware, updateCouponMiddleware, deleteCouponMiddleware, couponListMiddleware, validateCouponMiddleware } from "../modules/coupon/middlewares";

export default {
  Query: {
    couponList: couponListMiddleware,
    validateCoupon: validateCouponMiddleware
  },
  Mutation: {
    createCoupon: createCouponMiddleware,
    updateCoupon: updateCouponMiddleware,
    deleteCoupon: deleteCouponMiddleware,
  },
};