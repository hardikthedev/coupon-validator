import { validateCouponData } from "../../../utils/validateCouponData"

const createCouponMiddleware = async (resolve, parent, args, ctx, info) => {
  try {
    const { couponCode } = args.data
    const coupon = await ctx.prisma.coupon.findUnique({
      where: { couponCode },
      select: { couponCode: true },
    })
    if (coupon) {
      return new Error('Same coupon already exists!')
    }
    const validationError = validateCouponData(args.data)
    if (validationError) { return validationError }
    return resolve(parent, args, ctx, info);
  } catch (exec) {
    return new Error('Something went wrong')
  }
}

export default createCouponMiddleware