import { validateCouponData } from "../../../utils/validateCouponData"

const updateCouponMiddleware = async (resolve, parent, args, ctx, info) => {
  try {
    const { couponCode } = args.where
    const { couponCode: updatedCouponCode } = args.data
    const couponExists = await ctx.prisma.coupon.findUnique({
      where: { couponCode },
      select: { couponCode: true, id: true },
    })
    if (!couponExists) {
      return new Error('No such coupon exists')
    }
    const coupon = await ctx.prisma.coupon.findMany({
      where: { couponCode: updatedCouponCode, NOT: { id: couponExists.id } },
      select: { couponCode: true },
    })
    if (coupon.length) {
      return new Error('Same coupon already exists!')
    }
    const validationError = validateCouponData(args.data)
    if (validationError) { return validationError }
    return resolve(parent, args, ctx, info);
  } catch (exec) {
    return new Error('Something went wrong')
  }
}

export default updateCouponMiddleware