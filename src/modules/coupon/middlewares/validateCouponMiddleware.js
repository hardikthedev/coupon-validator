const validateCouponMiddleware = async (resolve, parent, args, ctx, info) => {
  try {
    const now = new Date().toISOString()
    args.where.validFrom = { lte: now }
    args.where.validTo = { gte: now }
    return resolve(parent, args, ctx, info);
  } catch (exec) {
    console.log(exec)
    return new Error('Something went wrong')
  }
}

export default validateCouponMiddleware