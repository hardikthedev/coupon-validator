const couponListMiddleware = async (resolve, parent, args, ctx, info) => {
  try {
    const { take } = args
    const now = new Date().toISOString()
    if (!take || take > 10) {
      args.take = 10
    }
    args.orderBy = { createdAt: 'desc' }
    args.where = { validFrom: { lte: now }, validTo: { gte: now } }
    return resolve(parent, args, ctx, info);
  } catch (exec) {
    return new Error('Something went wrong')
  }
}

export default couponListMiddleware