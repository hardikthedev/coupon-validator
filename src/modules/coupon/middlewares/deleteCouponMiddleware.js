const deleteCouponMiddleware = async (resolve, parent, args, ctx, info) => {
  try {
    const { couponCode } = args.where
    const couponExists = await ctx.prisma.coupon.findUnique({
      where: { couponCode },
      select: { couponCode: true },
    })
    if (!couponExists) {
      return new Error('No such coupon exists')
    }
    return resolve(parent, args, ctx, info);
  } catch (exec) {
    return new Error('Something went wrong')
  }
}

export default deleteCouponMiddleware