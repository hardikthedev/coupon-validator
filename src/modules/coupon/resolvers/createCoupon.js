const createCoupon = (parent, args, ctx, info) => {
  try {
    return ctx.prisma.coupon.create(args);
  } catch (exec) {
    throw exec
  }
}

export default createCoupon