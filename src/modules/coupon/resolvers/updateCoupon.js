const updateCoupon = (parent, args, ctx, info) => {
  try {
    return ctx.prisma.coupon.update(args);
  } catch (exec) {
    throw exec
  }
}

export default updateCoupon