const deleteCoupon = async (parent, args, ctx, info) => {
  try {
    await ctx.prisma.coupon.delete(args);
    return {
      message: "Coupon deleted successfully"
    }
  } catch (exec) {
    throw exec
  }
}

export default deleteCoupon