const couponList = (parent, args, ctx, info) => {
  try {
    return ctx.prisma.coupon.findMany(args);
  } catch (exec) {
    throw exec
  }
}

export default couponList