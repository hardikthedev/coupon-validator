const validateCoupon = async (parent, args, ctx, info) => {
  try {
    const { cartAmount } = args
    const coupons = await ctx.prisma.coupon.findMany({ where: args.where })
    if (!coupons.length) {
      throw new Error('Invalid or expired coupon')
    }
    const coupon = coupons[0]
    const { discountType, flatValue, percentValue, maxAmount } = coupon
    let discountedAmount = 0
    switch (discountType) {
      case 'FLAT':
        discountedAmount = flatValue <= cartAmount ? cartAmount - flatValue : 0
        break
      case 'PERCENT':
        let discount = (percentValue * cartAmount) / 100
        discount = discount >= maxAmount ? maxAmount : discount
        discountedAmount = discount <= cartAmount ? cartAmount - discount : 0
        break
      default:
        break
    }
    return {
      message: `Coungratulations, you saved ${cartAmount - discountedAmount} rupees on your cart! you only have to pay ${discountedAmount} now`,
      discountedAmount,
      cartAmount
    }
  } catch (exec) {
    throw exec
  }
}

export default validateCoupon