import { createCoupon, updateCoupon, deleteCoupon, couponList, validateCoupon } from "../modules/coupon/resolvers";
export default {
  Query: {
    couponList,
    validateCoupon
  },
  Mutation: {
    createCoupon,
    updateCoupon,
    deleteCoupon
  }
};