import * as path from 'path'
import { fileLoader } from 'merge-graphql-schemas'

const resolverArray = fileLoader(path.join(__dirname, './*.resolvers.*'))

export default resolverArray
