export const validateCouponData = (data) => {
  try {
    const { discountType, flatValue, percentValue, description, maxAmount } = data
    if (!description) {
      data.description = ""
    }
    switch (discountType) {
      case 'FLAT':
        if (!flatValue || flatValue <= 0) {
          throw new Error('Invalid flat discount value')
        }
        data.percentValue = 0
        data.maxAmount = 0
        break
      case 'PERCENT':
        if (!percentValue || percentValue < 0 || percentValue > 100) {
          throw new Error('Invalid discount percentage')
        }
        if (!maxAmount) {
          throw new Error('Please enter max amount for percentaged discount')
        }
        data.flatValue = 0
        break
      default:
        break
    }
  } catch (exec) {
    return exec
  }
}