import * as path from 'path'
import { importSchema } from 'graphql-import'
import { fileLoader, mergeTypes } from 'merge-graphql-schemas'

const typesArray = [...fileLoader(path.join(__dirname, './*.graphql'))]

export default mergeTypes(typesArray)