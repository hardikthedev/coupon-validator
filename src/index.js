import { PrismaClient } from './generated/client/index'
import { ApolloServer } from 'apollo-server'
import typeDefs from './graphql'
import resolvers from './resolvers'
import middlewares from './middlewares'
import { applyMiddleware } from 'graphql-middleware'
import { makeExecutableSchema } from 'graphql-tools'

const prisma = new PrismaClient()

const schema = makeExecutableSchema({ typeDefs, resolvers, })

const schemaWithMiddleware = applyMiddleware(schema, ...middlewares)

const server = new ApolloServer({
  schema: schemaWithMiddleware,
  introspection: true,
  playground: true,
  context: req => ({ ...req, prisma: prisma }),
});

server.listen(process.env.PORT || 4000).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});